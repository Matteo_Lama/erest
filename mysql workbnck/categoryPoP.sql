DELETE FROM category WHERE idcategory > -1;

INSERT INTO category(name)
VALUES('Antipasti');

INSERT INTO category(name)
VALUES('Primi');

INSERT INTO category(name)
VALUES('Secondi');

INSERT INTO category(name)
VALUES('Contorni');

INSERT INTO category(name)
VALUES('Bevande');

SELECT * FROM category;

DELETE FROM allergen WHERE idallergen > -1;

INSERT INTO allergen(name, description)
VALUES('Glutine', 'cereali, grano, segale, orzo, avena, farro, kamut, inclusi ibridati e derivati');

INSERT INTO allergen(name, description)
VALUES('Crostacei e derivati', 'sia quelli marini che d’acqua dolce: gamberi, scampi, aragoste, granchi, paguri e simili');

INSERT INTO allergen(name, description)
VALUES('Uova e derivati', 'tutti i prodotti composti con uova, anche in parte minima. Tra le più comuni: maionese, frittata, emulsionanti, pasta all’uovo, biscotti e torte anche salate, gelati e creme ecc');

INSERT INTO allergen(name, description)
VALUES('Pesce e derivati', 'inclusi i derivati, cioè tutti quei prodotti alimentari che si compongono di pesce, anche se in piccole percentuali');

INSERT INTO allergen(name, description)
VALUES('Arachidi e derivati', 'snack confezionati, creme e condimenti in cui vi sia anche in piccole dosi');

INSERT INTO allergen(name, description)
VALUES('Soia e derivati', 'latte, tofu, spaghetti, etc.');

INSERT INTO allergen(name, description)
VALUES('Latte e derivati', 'yogurt, biscotti e torte, gelato e creme varie. Ogni prodotto in cui viene usato il latte');

INSERT INTO allergen(name, description)
VALUES('Frutta a guscio e derivati', 'tutti i prodotti che includono: mandorle, nocciole, noci comuni, noci di acagiù, noci pecan e del Brasile e Queensland, pistacchi');

INSERT INTO allergen(name, description)
VALUES('Sedano e derivati', 'presente in pezzi ma pure all’interno di preparati per zuppe, salse e concentrati vegetali');

INSERT INTO allergen(name, description)
VALUES('Senape e derivati', 'si può trovare nelle salse e nei condimenti, specie nella mostarda');

INSERT INTO allergen(name, description)
VALUES('Semi di sesamo e derivati', 'oltre ai semi interi usati per il pane, possiamo trovare tracce in alcuni tipi di farine');

INSERT INTO allergen(name, description)
VALUES('Anidride solforosa e solfiti', 'usati come conservanti, possiamo trovarli in: conserve di prodotti ittici, in cibi sott’aceto, sott’olio e in salamoia, nelle marmellate, nell’aceto, nei funghi secchi e nelle bibite analcoliche');

INSERT INTO allergen(name, description)
VALUES('Lupino e derivati', 'presente ormai in molti cibi vegan, sotto forma di arrosti, salamini, farine e similari che hanno come base questo legume, ricco di proteine');

INSERT INTO allergen(name, description)
VALUES('Molluschi e derivati', 'canestrello, cannolicchio, capasanta, cuore, dattero di mare, fasolaro, garagolo, lumachino, cozza, murice, ostrica, patella, tartufo di mare, tellina e vongola etc.');

SELECT * FROM allergen;

DELETE FROM dish WHERE iddish > -1;

INSERT INTO dish(name, description, cost, idcategory)
VALUES('Tagliere di antipasti', 'Tagliere con piadina, salumi e formaggi','6.00', '1');

INSERT INTO dish(name, description, cost, idcategory)
VALUES('Crostini', 'Crostini con salse','4.00', '1');

INSERT INTO dish(name, description, cost, idcategory)
VALUES('Tagliatelle al ragu\'', 'Ragu\' di maiale','7.00', '2');

INSERT INTO dish(name, description, cost, idcategory)
VALUES('Strozzapreti', 'Panne e speck','6.50', '2');

INSERT INTO dish(name, description, cost, idcategory)
VALUES('Grigliata mista', 'Piatto con Salciccia, pancietta e lonza','8.50', '3');

INSERT INTO dish(name, description, cost, idcategory)
VALUES('Tagliata di manzo', 'Manzo marchigiano','9.50', '3');

INSERT INTO dish(name, description, cost, idcategory)
VALUES('Insalata mista', 'Con verdura di stagione','4.50', '4');

INSERT INTO dish(name, description, cost, idcategory)
VALUES('Patate al Forno', 'Patete insaporite','5.00', '4');

INSERT INTO dish(name, description, cost, idcategory)
VALUES('Acqua', 'Frizzanto e\\o Naturale','1.00', '5');

INSERT INTO dish(name, description, cost, idcategory)
VALUES('Vino della casa', 'Bianco e rosso','1.00', '5');

INSERT INTO dish(name, description, cost, idcategory)
VALUES('caffe\'', ' ','1.00', '5');

SELECT * FROM dish;

DELETE FROM `table` WHERE idtable > -1;

INSERT INTO `table`(number)
VALUES('1');

INSERT INTO `table`(number)
VALUES('2');

INSERT INTO `table`(number)
VALUES('3');

INSERT INTO `table`(number)
VALUES('4');

INSERT INTO `table`(number)
VALUES('5');

INSERT INTO `table`(number)
VALUES('6');

INSERT INTO `table`(number)
VALUES('7');

INSERT INTO `table`(number)
VALUES('8');

INSERT INTO `table`(number)
VALUES('9');

INSERT INTO `table`(number)
VALUES('10');

INSERT INTO `table`(number)
VALUES('11');

INSERT INTO `table`(number)
VALUES('12');

INSERT INTO `table`(number)
VALUES('13');

INSERT INTO `table`(number)
VALUES('14');

INSERT INTO `table`(number)
VALUES('15');

INSERT INTO `table`(number)
VALUES('16');

INSERT INTO `table`(number)
VALUES('17');

INSERT INTO `table`(number)
VALUES('18');

INSERT INTO `table`(number)
VALUES('19');

INSERT INTO `table`(number)
VALUES('20');

SELECT * FROM `table`;