package utilities;

public enum Category {
	
ANTIPASTI("Antipasto"),
PRIMI("Primi"),
SECONDI("Secondi"),
CONTORNI("Contorni"),
DOLCI("Dolci"),
BEVANDE("Bevande");

private String string;

Category(String string){
	this.string = string;
}

public String getString() {
	return string;
}

}
