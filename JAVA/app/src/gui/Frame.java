package gui;

import javax.swing.JFrame;

import database.ConnectionJDMC;

public class Frame extends JFrame{
	private ConnectionJDMC connectionJDMC;
	private Panel panel;
	
	public Frame(ConnectionJDMC connectionJDMC) {
		this.connectionJDMC = connectionJDMC;
		panel = new Panel();
		
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		this.setBounds(0,  0, 500, 600);
		this.panel.setBounds(this.getBounds());
		
		this.panel.setLayout(null);
		
		this.add(this.panel);
		this.panel.setVisible(true);
		this.setVisible(true);
		
		
	}

}
