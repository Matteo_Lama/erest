package table;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

import javax.swing.JTable;

public class TableControl {
	private JTable jTable;
	
	public TableControl() {
		jTable = new JTable();
	}
	
	public void upDate(ResultSet result) throws Throwable {
		String table_field[];
		
		ResultSetMetaData rmd = result.getMetaData();
		
		int nr_row = result.last() ?  result.getRow() : 0;
		
		Object data[][] = new Object[nr_row][];
		
		int col_nr = rmd.getColumnCount();
		table_field = new String[col_nr];
		for(int i=1;i<col_nr;i++) {
			table_field[i-1]= rmd.getColumnName(i);
		}
		
		result.beforeFirst();
		while(result.next()) {
			int row= result.getRow();
			data[row-1]= new Object[col_nr];
			for(int i= 0; i<col_nr;i++) {
				data[row-1][i]=result.getString(i+1);
			}
		}
	}
	public JTable getJTable() {
		return jTable;
	}
	
}
