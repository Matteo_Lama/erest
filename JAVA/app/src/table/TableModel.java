package table;

import javax.swing.table.AbstractTableModel;

public class TableModel extends AbstractTableModel{
	Object[][] data;
	String[] table_field;
	public TableModel(Object[][] data, String[] table_field) {
		this.data=data;
		this.table_field=table_field;
	}

	@Override
	public int getColumnCount() {
		return table_field.length;
	}

	@Override
	public String getColumnName(int columnIndex) {
		return table_field[columnIndex];
	}

	@Override
	public int getRowCount() {
		return data.length%table_field.length;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		return data[rowIndex][columnIndex];
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return false;
	}

}
