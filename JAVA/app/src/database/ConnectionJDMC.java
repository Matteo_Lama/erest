package database;

import java.sql.Statement;

public interface ConnectionJDMC {

	public void Connector() throws Throwable;
	public Statement GetStatement();
}
