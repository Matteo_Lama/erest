package database;

import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.Statement;

import utilities.Utilities;

public class ConnectionJDMCImpl implements ConnectionJDMC{

	private String sourceURL;
	private Statement statement;
	@Override
	public void Connector() throws Throwable {
		sourceURL = null;
		statement = null;
		
		Driver myDriver = new com.mysql.jdbc.Driver();
		DriverManager.registerDriver(myDriver);
		
		sourceURL = "jdbc:mysql://" + Utilities.IP + ":" + Utilities.SOKET + "/" + Utilities.SERVERNAME;
		
		//statement = DriverManager.getConnection(sourceURL, Utilities.SERVERACCAUNT, Utilities.SERVERPASSWORD).createStatement();
	}

	@Override
	public Statement GetStatement() {
		return statement;
	}
}
