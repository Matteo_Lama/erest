package app;

import database.ConnectionJDMC;
import database.ConnectionJDMCImpl;
import gui.Frame;

public class MainERestaurant {
	private static ConnectionJDMC connectionJDMC;
	private static Frame frame;
	
	public static void main(String[] args) throws Throwable {
		connectionJDMC = new ConnectionJDMCImpl();
		connectionJDMC.Connector();
		frame = new Frame(connectionJDMC);

	}

}
